package main

import (
	"encoding/json"
	"flag"
	"io/ioutil"
	"log"
	"os"
)

var (
	ConfigFile string
	Conf       *Config
)

func init() {
	flag.StringVar(&ConfigFile, "conf", "~/.vcsrc", "Change config file")
	flag.Parse()

	LoadConf()
}

type Project struct {
	VCS  string `json:"vcs"`
	URL  string `json:"url"`
	Path string `json:"path"`
}

type Config struct {
	WorkDir  string    `json:"workdir"`
	Projects []Project `json:"projects"`
}

func LoadConf() (*Config, error) {
	var config *Config

	f, err := os.Open(ConfigFile)
	if err != nil {
		log.Printf("Config file %s not found", ConfigFile)
	}
	if f != nil {
		if err = json.NewDecoder(f).Decode(&config); err != nil {
			return nil, err
		}
		defer f.Close()
	}

	return config, nil
}

func WriteConf() error {
	b, err := json.Marshal(Conf)
	if err != nil {
		return err
	}
	return ioutil.WriteFile(ConfigFile, b, 0644)
}

func main() {
	defer WriteConf()
}
